package com.primetoxinz;

import betterwithmods.common.blocks.BlockDirtSlab;
import com.google.common.collect.AbstractIterator;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.DimensionType;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraftforge.event.terraingen.PopulateChunkEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import org.apache.logging.log4j.Logger;

import javax.annotation.Nonnull;
import java.util.Iterator;

@Mod.EventBusSubscriber
@Mod(modid = BWT.MODID, name = BWT.NAME, version = BWT.VERSION, dependencies =  BWT.DEPENDENCIES)
public class BWT {
    public static final String MODID = "betterwithterrain";
    public static final String NAME = "Better With Terrain";
    public static final String VERSION = "1.0";
    public static final String DEPENDENCIES = "required-after:betterwithmods";
    @GameRegistry.ObjectHolder("betterwithmods:dirt_slab")
    public static BlockDirtSlab dirt_slab = null;
    private static Logger logger;

    @SubscribeEvent
    public static void onPopulateChunkPreEvent(PopulateChunkEvent.Pre event) {
        World world = event.getWorld();
        if (world.provider.getDimensionType() != DimensionType.OVERWORLD || dirt_slab == null)
            return;
        IChunkProvider chunkProvider = world.getChunkProvider();
        int x = event.getChunkX() << 4;
        int z = event.getChunkZ() << 4;
        int offX = 16;
        int offZ = 16;
        if (chunkProvider.getLoadedChunk(event.getChunkX() - 1, event.getChunkZ()) != null) {
            x--;
        } else {
            x++;
        }
        if (chunkProvider.getLoadedChunk(event.getChunkX(), event.getChunkZ() - 1) != null) {
            z--;
        } else {
            z++;
        }
        if (chunkProvider.getLoadedChunk(event.getChunkX() + 1, event.getChunkZ()) != null)
            offX++;
        else
            offX--;
        if (chunkProvider.getLoadedChunk(event.getChunkX(), event.getChunkZ() + 1) != null)
            offZ++;
        else
            offZ--;
        IBlockState grass_slab = dirt_slab.getDefaultState().withProperty(BlockDirtSlab.VARIANT, BlockDirtSlab.DirtSlabType.GRASS);
        posLoop:
        for (ModifiableBlockPos pos : getBox(new BlockPos(x + 8, 0, z + 8), offX, 1, offZ)) {
            while (pos.getY() < 255 && world.getBlockState(pos).getMaterial() != Material.GRASS) {
                pos.up();
            }
            if (pos.getY() >= 255) {
                continue;
            }

            for (EnumFacing face : EnumFacing.HORIZONTALS) {
                pos.offset(face, 1);
                Block block = world.getBlockState(pos).getBlock();
                if (block.isReplaceable(world, pos) || block == dirt_slab)
                    continue posLoop;
                pos.offset(face, -1);
            }

            pos.up();
            IBlockState replaceState = world.getBlockState(pos);
            if (!replaceState.getBlock().isReplaceable(world, pos) || replaceState.getBlock() == Blocks.WATER)
                continue;

            for (EnumFacing face : EnumFacing.HORIZONTALS) {
                pos.offset(face, 1);
                if (world.getBlockState(pos).getBlock() == Blocks.GRASS) {
                    pos.offset(face, -1);
                    world.setBlockState(pos, grass_slab, 2);
                    pos.up();
                    IBlockState upperBlockState = world.getBlockState(pos);
                    if (!upperBlockState.getBlock().isAir(upperBlockState, world, pos) &&
                            replaceState.getBlock() == upperBlockState.getBlock()) {
                        world.setBlockState(pos, Blocks.AIR.getDefaultState(), 2);
                    }
                    continue posLoop;
                }
                pos.offset(face, -1);
            }
        }
    }

    private static Iterable<ModifiableBlockPos> getBox(final BlockPos posStart, int xOff, int yOff,
                                                       int zOff) {
        return getBox(posStart, posStart.add(xOff - 1, yOff - 1, zOff - 1));
    }

    private static Iterable<ModifiableBlockPos> getBox(final BlockPos posStart,
                                                      final BlockPos posEnd) {
        return new Iterable<ModifiableBlockPos>() {

            @Override
            @Nonnull
            public Iterator<ModifiableBlockPos> iterator() {
                return new AbstractIterator<ModifiableBlockPos>() {
                    private int currX = posStart.getX();
                    private int currY = posStart.getY();
                    private int currZ = posStart.getZ();

                    private ModifiableBlockPos pos;

                    @Override
                    protected ModifiableBlockPos computeNext() {
                        if (pos == null)
                            return (pos = new ModifiableBlockPos(posStart));
                        if (currX == posEnd.getX() && currY == posEnd.getY()
                                && currZ == posEnd.getZ())
                            return this.endOfData();

                        if (currX < posEnd.getX())
                            currX++;
                        else if (currZ < posEnd.getZ()) {
                            currX = posStart.getX();
                            currZ++;
                        } else if (currY < posEnd.getY()) {
                            currX = posStart.getX();
                            currZ = posStart.getZ();
                            currY++;
                        }
                        pos.set(currX, currY, currZ);
                        return pos;
                    }
                };
            }
        };
    }

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        logger = event.getModLog();
    }

    public static class ModifiableBlockPos extends BlockPos {

        private int x;
        private int y;
        private int z;

        public ModifiableBlockPos(int x, int y, int z) {
            super(0, 0, 0);
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public ModifiableBlockPos(BlockPos blockPos) {
            this(blockPos.getX(), blockPos.getY(), blockPos.getZ());
        }

        @Override
        public int getX() {
            return x;
        }

        @Override
        public int getY() {
            return y;
        }

        @Override
        public int getZ() {
            return z;
        }

        @Override
        public BlockPos offset(EnumFacing facing, int n) {
            x = x + facing.getFrontOffsetX() * n;
            y = y + facing.getFrontOffsetY() * n;
            z = z + facing.getFrontOffsetZ() * n;
            return this;
        }

        public ModifiableBlockPos set(int x, int y, int z) {
            this.x = x;
            this.y = y;
            this.z = z;
            return this;
        }
    }
}
